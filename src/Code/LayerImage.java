package Code;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;

import javax.imageio.ImageIO;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class LayerImage implements Serializable {

    private transient Image canvasImage;
    private String name;

    public LayerImage(String name, Image canvasImage) {
        this.name = name;
        this.canvasImage = canvasImage;
    }

    private void readObject(ObjectInputStream inputStream) throws IOException, ClassNotFoundException {
        inputStream.defaultReadObject();
        // Read in the image
        canvasImage = SwingFXUtils.toFXImage(ImageIO.read(inputStream), null);
    }

    private void writeObject(ObjectOutputStream outputStream) throws IOException {
        outputStream.defaultWriteObject();
        // Write the image to the file.
        ImageIO.write(SwingFXUtils.fromFXImage(canvasImage, null), "png", outputStream);
    }

    public javafx.scene.image.Image getCanvasImage() {
        return canvasImage;
    }

    public String getName() {
        return name;
    }
}
