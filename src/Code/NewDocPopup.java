package Code;

import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;

public class NewDocPopup {

    private String name;
    private int width, height;
    private boolean valuesSet = false;

    public Document show() {
        try {

            FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource("/Layout/newFileWindow.fxml"));
            GridPane root = fxmlLoader.load();
            Scene newDocWinScene = new Scene(root);
            Stage stage = new Stage();
            stage.setWidth(400);
            stage.setHeight(190);
            stage.setScene(newDocWinScene);
            stage.setTitle("New Document");
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initStyle(StageStyle.UNDECORATED);

            GridPane content = new GridPane();
            content.getStyleClass().add("contentPane");

            Label docNameLbl = new Label("Document Name:");
            TextField docNameTxtF = new TextField("Untitled");

            Label docWidthLbl = new Label("Document Width:");
            TextField docWidthTxtF = new TextField("256");

            Label docHeightLbl = new Label("Document Height:");
            TextField docHeightTxtF = new TextField("256");

            HBox btnHbox = new HBox();
            btnHbox.setAlignment(Pos.CENTER);
            GridPane.setColumnSpan(btnHbox, 2);
            btnHbox.setSpacing(25);

            Button okBtn = new Button("OK");
            Button cancelBtn = new Button("Cancel");

            //Events for buttons
            okBtn.setOnAction(event -> {
                name = docNameTxtF.getText();

                if(docWidthTxtF.getText().isEmpty()) {
                    docWidthTxtF.setText("1");
                }

                if(docHeightTxtF.getText().isEmpty()) {
                    docHeightTxtF.setText("1");
                }

                width = Integer.valueOf(docWidthTxtF.getText());
                height = Integer.valueOf(docHeightTxtF.getText());
                valuesSet = true;
                stage.close();
            });

            cancelBtn.setOnAction(event -> {
                valuesSet = false;
                stage.close();
            } );

            btnHbox.getChildren().addAll(okBtn, cancelBtn);

            GridPane.setRowIndex(docNameLbl, 0);
            GridPane.setColumnIndex(docNameLbl, 0);
            GridPane.setRowIndex(docNameTxtF, 0);
            GridPane.setColumnIndex(docNameTxtF, 1);

            GridPane.setRowIndex(docWidthLbl, 1);
            GridPane.setColumnIndex(docWidthLbl, 0);
            GridPane.setRowIndex(docWidthTxtF, 1);
            GridPane.setColumnIndex(docWidthTxtF, 1);

            GridPane.setRowIndex(docHeightLbl, 2);
            GridPane.setColumnIndex(docHeightLbl, 0);
            GridPane.setRowIndex(docHeightTxtF, 2);
            GridPane.setColumnIndex(docHeightTxtF, 1);

            GridPane.setRowIndex(btnHbox, 3);
            GridPane.setColumnIndex(btnHbox, 0);

            content.getChildren().addAll(docNameLbl, docNameTxtF, docWidthLbl, docWidthTxtF, docHeightLbl, docHeightTxtF, btnHbox);

            content.setPadding(new Insets(0, 10, 0, 10));
            content.setHgap(10);
            content.setVgap(10);
            GridPane.setHgrow(content, Priority.ALWAYS);
            content.setAlignment(Pos.CENTER);

            root.add(content, 0, 1);

            // Listeners to ensure values remain numeric
            docWidthTxtF.textProperty().addListener((observable, oldValue, newValue) -> valChanged(docWidthTxtF, oldValue, newValue));
            docHeightTxtF.textProperty().addListener((observable, oldValue, newValue) -> valChanged(docHeightTxtF, oldValue, newValue));

            stage.showAndWait();

        } catch (IOException e) {
            e.printStackTrace();
        }

        if(valuesSet) {

            return new Document(name, width, height);

        } else return null;

    }

    private void valChanged(TextField textField, String oldValue, String newValue) {

        // Make sure the new value is a non-zero number
        if(!newValue.isEmpty() && !isInt(newValue) && newValue != "0") {
            textField.setText(oldValue);
        }

    }

    //Checks if a string is an integer.
    private static boolean isInt(String str) {
        try {
            int i = Integer.parseInt(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

}
