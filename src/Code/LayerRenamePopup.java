package Code;

import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;

public class LayerRenamePopup {

    private boolean renamed = false;
    private String name;


    public String show(String oldName) {

        try {
            FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource("/Layout/renameLayerWindow.fxml"));
            GridPane root = fxmlLoader.load();
            Scene newDocWinScene = new Scene(root);
            Stage stage = new Stage();
            stage.setWidth(400);
            stage.setHeight(110);
            stage.setScene(newDocWinScene);
            stage.setTitle("Rename Layer");
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initStyle(StageStyle.UNDECORATED);

            GridPane content = new GridPane();
            content.setVgap(10);
            content.setHgap(10);
            content.setAlignment(Pos.CENTER);

            Label layerNameLbl = new Label("Layer Name:");
            TextField layerNameTxtF = new TextField(oldName);

            HBox btnsHbx = new HBox();
            btnsHbx.setSpacing(25);

            Button okBtn = new Button("OK");
            Button cancelBtn = new Button("Cancel");

            // Events for the buttons.
            okBtn.setOnAction(event -> {

                // If the user hasn't set a name, it will reset back to what it was.
                if (!layerNameTxtF.getText().isEmpty()) {
                    name = layerNameTxtF.getText();
                    renamed = true;
                    stage.close();
                } else stage.close();

            });

            cancelBtn.setOnAction(event -> stage.close());

            btnsHbx.getChildren().addAll(okBtn, cancelBtn);

            // Add the controls to the content gridpane.
            content.add(layerNameLbl, 0, 0);
            content.add(layerNameTxtF, 1, 0);
            content.add(btnsHbx, 0, 1);

            GridPane.setColumnSpan(btnsHbx, 2);

            root.add(content, 0, 1);

            stage.showAndWait();


        } catch (IOException e) {
            e.printStackTrace();
        }

        // If the layer has been renamed, then return the new name, otherwise return old name.
        if(renamed) return name;
        else return oldName;

    }

}
