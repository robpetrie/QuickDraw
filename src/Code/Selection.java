package Code;

import javafx.geometry.Rectangle2D;

public class Selection {

    private double x, y, width, height;

    private Rectangle2D rect2D;

    Selection(double x, double y, double width, double height) {

        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;

    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    // Set the rectangle2D to the area defined by the class variables and return it.
    public Rectangle2D getRect2D() {

        rect2D = new Rectangle2D(x, y, width, height);

        return rect2D;
    }
}
