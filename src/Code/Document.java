package Code;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.Canvas;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

class Document {

    private String name;

    private int width, height;

    private double localMouseX, localMouseY;

    private Color fgColor = Color.BLACK;
    private Color bgColor = Color.WHITE;

    private Canvas toolCanvas;

    private String selectedTool;

    private List<Layer> layerList = new ArrayList<>();

    private int activeLayerIndex;

    private Selection selection;

    private double brushSize;

    private boolean shouldFillShapes;

    Document(String name, int width, int height) {
        this.name = name;
        this.width = width;
        this.height = height;

        brushSize = 1;
        shouldFillShapes = false;

        localMouseX = 0;
        localMouseY = 0;
        toolCanvas = new Canvas(width, height);
        toolCanvas.setMouseTransparent(true);
        toolCanvas.toFront();
        selectedTool = "move";

        selection = new Selection(0, 0, width, height);

        Layer defaultLayer = newLayer("Background");
        defaultLayer.getCanvas().getGraphicsContext2D().setFill(Color.WHITE);
        defaultLayer.getCanvas().getGraphicsContext2D().fillRect(0, 0, width, height);
        activeLayerIndex = 0;

    }

    Document(File file) throws IOException {

        Image img = SwingFXUtils.toFXImage(ImageIO.read(file), null);

        // Get values from img
        this.name = file.getName();
        this.width = (int)img.getWidth();
        this.height = (int)img.getHeight();

        // Make tool canvas.
        toolCanvas = new Canvas(width, height);
        toolCanvas.setMouseTransparent(true);
        toolCanvas.toFront();
        selectedTool = "move";

        selection = new Selection(0, 0, width, height);

        // Create a base layer and draw the image on it.
        Layer layer = newLayer(name);
        layer.getCanvas().getGraphicsContext2D().drawImage(img, 0, 0);

    }

    public List<Layer> getLayerList() {
        return layerList;
    }

    public double getLocalMouseX() {
        return localMouseX;
    }

    public double getLocalMouseY() {
        return localMouseY;
    }

    public void setLocalMouseX(double mouseX) {
        localMouseX = mouseX;
    }

    public void setLocalMouseY(double mouseY) {
        localMouseY = mouseY;
    }

    public Canvas getToolCanvas() {
        return toolCanvas;
    }

    public String getSelectedTool() {
        return selectedTool;
    }

    public void setSelectedTool(String tool) {
        selectedTool = tool;
    }

    public String getName() {
        return name;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public Color getFgColor() {
        return fgColor;
    }

    public void setFgColor(Color fgColor) {
        this.fgColor = fgColor;
    }

    public Color getBgColor() {
        return bgColor;
    }

    public void setBgColor(Color bgColor) {
        this.bgColor = bgColor;
    }

    public Layer newLayer(String name) {
        Layer layer = new Layer(this, name, width, height);

        layerList.add(0, layer);

        return layer;
    }

    public int getActiveLayerIndex() {
        return activeLayerIndex;
    }

    public void setActiveLayerIndex(int activeLayerIndex) {

        //Make all the layer mouse transparent.
        for (Layer layer : layerList) {
            layer.setMouseTransparent(true);
        }

        //Set the new active layer.
        this.activeLayerIndex = activeLayerIndex;

        //Set the new active layer to register mouse events.
        layerList.get(activeLayerIndex).setMouseTransparent(false);

    }

    public void removeLayerByIndex (int index) {

        // Ensure the layer list has more than one item in
        if (layerList.size() != 1) {
            if (index != 0) {
                layerList.remove(index);
                setActiveLayerIndex(index-1);
            } else {
                layerList.remove(index);
            }
        }

    }

    public Selection getSelection() {
        return selection;
    }

    public void setSelection(Selection sel) {
        selection = sel;
    }

    public double getBrushSize() {
        return brushSize;
    }

    public void setBrushSize(double brushSize) {
        this.brushSize = brushSize;
    }

    public boolean shouldFillShapes() {
        return shouldFillShapes;
    }

    public void setShouldFillShapes(boolean shouldFillShapes) {
        this.shouldFillShapes = shouldFillShapes;
    }

    public void saveDocAsImg(File file) throws IOException{

        //Setup the snapshot for transparency.
        SnapshotParameters params = new SnapshotParameters();
        params.setFill(Color.TRANSPARENT);

        ImageOutputStream out = ImageIO.createImageOutputStream(file);

        // Canvas that is used to flatten the layers.
        Canvas flatImgCanvas = new Canvas(width, height);

        // Store a reversed version of the layerList
        ListIterator listIterator = layerList.listIterator(layerList.size());
        ArrayList<Layer> reverseLayerList = new ArrayList<>();

        while (listIterator.hasPrevious()) {
            reverseLayerList.add((Layer)listIterator.previous());
        }

        // Iterate backward through the layer list and draw the layer contents to the flat canvas.
        for (Layer layer : reverseLayerList) {

            Image layerImg = layer.getCanvas().snapshot(params, null);

            flatImgCanvas.getGraphicsContext2D().drawImage(layerImg, 0,  0);

        }

        // Take a snapshot of the flat canvas
        Image img = flatImgCanvas.snapshot(params, null);

        // Write the snapshot to a file.
        ImageIO.write(SwingFXUtils.fromFXImage(img, null), "png", out);

        out.close();

    }

}
