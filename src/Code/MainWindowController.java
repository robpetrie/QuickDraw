package Code;

import javafx.application.Platform;
import javafx.css.PseudoClass;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.ImageCursor;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.*;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.FileChooser;

import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

public class MainWindowController extends WindowBase {

    private List<Document> documentList = new ArrayList<>();
    private int currentDocumentIndex = 0;

    @FXML
    private MenuItem newDocMenu, copyMenu, cutMenu, pasteMenu;

    @FXML
    private HBox topControlPanel;
    
    @FXML
    private TextField brushSizeTxt;

    @FXML
    private Slider brushSizeSlider;

    @FXML
    private CheckBox fillShapesChk;

    @FXML
    private Button fgColorBtn, bgColorBtn;

    @FXML
    private Slider rSlider, gSlider, bSlider;
    @FXML
    private TextField rSpinnerTxt, gSpinnerTxt, bSpinnerTxt;

    @FXML
    private boolean isFgColorSelected = true;

    @FXML
    private TabPane centrePane;

    @FXML
    private VBox layersVBox;

    @FXML
    private Button moveTool, boxSelectTool, pencilTool, lineTool, rectTool, ovalTool;

    private final Clipboard clipboard = Clipboard.getSystemClipboard();
    private final ClipboardContent clipboardContent = new ClipboardContent();

    private EventHandler<ActionEvent> layerChangeOrderEvent;

    @FXML
    private void initialize() {

        //Set the default values for window size
        setDefaultWidth(1000);
        setDefaultHeight(700);

        // Key Combos for menu items.
        newDocMenu.setAccelerator(new KeyCodeCombination(KeyCode.N, KeyCombination.SHORTCUT_DOWN));
        copyMenu.setAccelerator(new KeyCodeCombination(KeyCode.C, KeyCombination.SHORTCUT_DOWN));
        cutMenu.setAccelerator(new KeyCodeCombination(KeyCode.X, KeyCombination.SHORTCUT_DOWN));
        pasteMenu.setAccelerator(new KeyCodeCombination(KeyCode.V, KeyCombination.SHORTCUT_DOWN));

        //Event for moving the layer order.
        layerChangeOrderEvent = event -> {
            //Gets index of layer hbox in layer list.
            int index = layersVBox.getChildrenUnmodifiable().indexOf(((Button)event.getSource()).getParent().getParent());

            Document currentDoc = documentList.get(centrePane.getSelectionModel().getSelectedIndex());

            Node eventSrc = (Node) event.getSource();

            if (eventSrc.getId().equals("up") && index != 0) {

                //Swap the layer that has been clicked and the one above it, then reload the layer list.
                Collections.swap(documentList.get(currentDocumentIndex).getLayerList(), index, index - 1);

                // Set the current document's active layer index to the index of the layer that was clicked.
                currentDoc.setActiveLayerIndex(index-1);

            } else if (eventSrc.getId().equals("down") && index != documentList.get(currentDocumentIndex).getLayerList().size() - 1) {

                //Swap the layer that has been clicked and the one below it, then reload the layer list.
                Collections.swap(documentList.get(currentDocumentIndex).getLayerList(), index, index + 1);

                // Set the current document's active layer index to the index of the layer that was clicked.
                currentDoc.setActiveLayerIndex(index+1);

            }

            refreshCurrentTab();

        };

        // Listener for shape fill checkbox.
        fillShapesChk.selectedProperty().addListener((observable, oldValue, newValue) -> documentList.get(centrePane.getSelectionModel().getSelectedIndex()).setShouldFillShapes(newValue));

        // Red Spinner Listeners

        //Ensure valid values whilst value is being set.
        rSpinnerTxt.textProperty().addListener((observable, oldValue, newValue) -> spinnerChange(rSpinnerTxt, rSlider, oldValue, newValue));

        //Ensure no invalid values are left in place when the spinner is deselected.
        rSpinnerTxt.focusedProperty().addListener((observable, oldValue, newValue) -> spinnerFocusLost(rSpinnerTxt, newValue, 0));

        //Set the spinner value when the slider is changed.
        rSlider.valueProperty().addListener((observable, oldValue, newValue) -> sliderChanged(rSpinnerTxt, rSlider, newValue));


        // Green Spinner Listeners

        //Ensure valid values whilst value is being set.
        gSpinnerTxt.textProperty().addListener((observable, oldValue, newValue) -> spinnerChange(gSpinnerTxt, gSlider, oldValue, newValue));

        //Ensure no invalid values are left in place when the spinner is deselected.
        gSpinnerTxt.focusedProperty().addListener((observable, oldValue, newValue) -> spinnerFocusLost(gSpinnerTxt, newValue, 0));

        //Set the spinner value when the slider is changed.
        gSlider.valueProperty().addListener((observable, oldValue, newValue) -> sliderChanged(gSpinnerTxt, gSlider, newValue));


        // Blue Spinner Listeners

        //Ensure valid values whilst value is being set.
        bSpinnerTxt.textProperty().addListener((observable, oldValue, newValue) -> spinnerChange(bSpinnerTxt, bSlider, oldValue, newValue));

        //Ensure no invalid values are left in place when the spinner is deselected.
        bSpinnerTxt.focusedProperty().addListener((observable, oldValue, newValue) -> spinnerFocusLost(bSpinnerTxt, newValue, 0));

        //Set the spinner value when the slider is changed.
        bSlider.valueProperty().addListener((observable, oldValue, newValue) -> sliderChanged(bSpinnerTxt, bSlider, newValue));

        // Brush Size Spinner Listeners

        //Ensure valid values whilst value is being set.
        brushSizeTxt.textProperty().addListener((observable, oldValue, newValue) -> {
            spinnerChange(brushSizeTxt, brushSizeSlider, oldValue, newValue);
            updateBrushSize();
        });

        //Ensure no invalid values are left in place when the spinner is deselected.
        brushSizeTxt.focusedProperty().addListener((observable, oldValue, newValue) -> {
            spinnerFocusLost(brushSizeTxt, newValue, 1);
            updateBrushSize();
        });

        //Set the spinner value when the slider is changed.
        brushSizeSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            sliderChanged(brushSizeTxt, brushSizeSlider, newValue);
            updateBrushSize();
        });


        newDocument("test", 500, 500);
        newDocument("Untitled", 500, 500);

    }

    @FXML
    private void copySelection() {

        currentDocumentIndex = centrePane.getSelectionModel().getSelectedIndex();

        // Store the active layer for the current document in a variable.
        Layer activeLayer = documentList.get(currentDocumentIndex).getLayerList().get(documentList.get(currentDocumentIndex).getActiveLayerIndex());

        // Set the snapshot parameters with the dimensions and position of the selection rectangle from the document.
        SnapshotParameters params = new SnapshotParameters();
        params.setFill(Color.TRANSPARENT);
        params.setViewport(documentList.get(currentDocumentIndex).getSelection().getRect2D());

        // Take a snapshot from the active layer canvas.
        Image snapshot = activeLayer.getCanvas().snapshot(params, null);
        // Create and image selection from the snapshot to load into the clipboard.
        ImageSelection imgSel = new ImageSelection(SwingFXUtils.fromFXImage(snapshot, null));
        // Load the snapshot into the system clipboard.
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(imgSel, null);

    }

    @FXML
    private void cutSelection() {

        copySelection();

        currentDocumentIndex = centrePane.getSelectionModel().getSelectedIndex();

        // Store the active layer for the current document in a variable.
        Layer activeLayer = documentList.get(currentDocumentIndex).getLayerList().get(documentList.get(currentDocumentIndex).getActiveLayerIndex());

        // Get the area of the selection.
        Rectangle2D selection = documentList.get(currentDocumentIndex).getSelection().getRect2D();

        // Clear the selection area from the active layer.
        activeLayer.getCanvas().getGraphicsContext2D().clearRect(selection.getMinX(), selection.getMinY(), selection.getWidth(), selection.getHeight());

    }

    @FXML
    private void pasteSelection() {

        currentDocumentIndex = centrePane.getSelectionModel().getSelectedIndex();

        Document curDoc = documentList.get(currentDocumentIndex);

        try {

            // Store the image from the clipboard.
            Image clipboardImg = SwingFXUtils.toFXImage(getImageFromClipboard(), null);

            String layerName = curDoc.getLayerList().get(curDoc.getActiveLayerIndex()).getName() + " Copy";

            // Check if pasted image is too large to fit natively on canvas.
            if (curDoc.getWidth() < clipboardImg.getWidth() || curDoc.getHeight() < clipboardImg.getHeight()) {

                PasteImgPopup pasteImgPopup = new PasteImgPopup();

                String choice = pasteImgPopup.show();

                Layer layer;

                switch (choice) {

                    case "crop":

                        // Make new layer for image to go on.
                        layer = curDoc.newLayer(layerName);
                        // Draw clipboard contents to layer.
                        layer.getCanvas().getGraphicsContext2D().drawImage(clipboardImg, 0, 0);

                        break;

                    case "resizeImg":

                        // Make new layer for image to go on.
                        layer = curDoc.newLayer(layerName);

                        // Calculate which dimension the image should be scaled with.
                        if (curDoc.getWidth() > curDoc.getHeight()) {

                            double resizeScale = clipboardImg.getHeight() / curDoc.getHeight();
                            double newWidth = clipboardImg.getWidth() / resizeScale;

                            // Draw clipboard contents to layer.
                            layer.getCanvas().getGraphicsContext2D().drawImage(clipboardImg, 0, 0, newWidth, curDoc.getHeight());

                        } else {

                            double resizeScale = clipboardImg.getWidth() / curDoc.getWidth();
                            double newHeight = clipboardImg.getHeight() / resizeScale;

                            // Draw clipboard contents to layer.
                            layer.getCanvas().getGraphicsContext2D().drawImage(clipboardImg, 0, 0, curDoc.getWidth(), newHeight);

                        }

                        break;

                }

            } else {

                // Make new layer for image to go on.
                Layer layer = curDoc.newLayer(layerName);
                // Draw clipboard contents to layer.
                layer.getCanvas().getGraphicsContext2D().drawImage(clipboardImg, 0, 0);

            }

            refreshCurrentTab();

        } catch (Exception ignored) {
        }

    }

    private java.awt.image.BufferedImage getImageFromClipboard()
            throws Exception
    {
        Transferable transferable = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null);
        if (transferable != null && transferable.isDataFlavorSupported(DataFlavor.imageFlavor))
        {
            return toBufferedImage((java.awt.Image) transferable.getTransferData(DataFlavor.imageFlavor));
        }
        else
        {
            return null;
        }
    }

    public static BufferedImage toBufferedImage(java.awt.Image img)
    {
        if (img instanceof BufferedImage)
        {
            return (BufferedImage) img;
        }

        // Create a buffered image with transparency
        BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

        // Draw the image on to the buffered image
        Graphics2D bGr = bimage.createGraphics();
        bGr.drawImage(img, 0, 0, null);
        bGr.dispose();

        // Return the buffered image
        return bimage;
    }

    private void spinnerChange(TextField spinner, Slider slider, String oldValue, String newValue) {

        //Check new value is an integer, not empty, and remains within the bounds of the slider's ranger
        if ((!isInt(newValue) || Integer.valueOf(newValue) < slider.getMin() || Integer.valueOf(newValue) > slider.getMax()) && !newValue.isEmpty()) {
            spinner.setText(oldValue);
        } else {

            //Set the slider's value from the value of the spinner, provided it's not empty.
            if (!newValue.isEmpty()) {

                slider.setValue(Integer.parseInt(newValue));

            } else {
                slider.setValue(0);
            }

            updateColors();

        }

    }

    private void spinnerFocusLost(TextField spinner, Boolean newValue, int minVal) {

        if (!newValue) {
            if (spinner.getText().isEmpty()) {
                spinner.setText(String.valueOf(minVal));
            }

            //Remove any leading zeros from the text.
            spinner.setText(String.valueOf(Integer.valueOf(spinner.getText())));

            updateColors();

        }

    }

    private void sliderChanged(TextField spinner, Slider slider, Number newValue) {

        //Only change the value of the spinner if it's not empty and the slider is the focus.
        if (!spinner.getText().isEmpty() && slider.isFocused()) {
            spinner.setText(String.valueOf(newValue.intValue()));
        }

    }



    //Events for selecting the tools.
    @FXML
    private void selectMoveTool() {
        deselectOtherTools();
        moveTool.setStyle("-fx-background-image: url('/res/tools/moveWhite.png'); -fx-background-color: #0a0a0a;");
        documentList.get(currentDocumentIndex).setSelectedTool("move");
    }

    @FXML
    private void selectBoxSelectTool() {
        deselectOtherTools();
        boxSelectTool.setStyle("-fx-background-image: url('/res/tools/boxSelectWhite.png'); -fx-background-color: #0a0a0a;");
        documentList.get(currentDocumentIndex).setSelectedTool("boxSelect");

    }

    @FXML
    private void selectPencilTool() {
        deselectOtherTools();
        pencilTool.setStyle("-fx-background-image: url('/res/tools/pencilWhite.png'); -fx-background-color: #0a0a0a;");
        documentList.get(currentDocumentIndex).setSelectedTool("pencil");
        setBrushCursor();
    }

    @FXML
    private void selectLineTool() {
        deselectOtherTools();
        lineTool.setStyle("-fx-background-image: url('/res/tools/lineWhite.png'); -fx-background-color: #0a0a0a;");
        documentList.get(currentDocumentIndex).setSelectedTool("line");
    }

    @FXML
    private void selectRectTool() {
        deselectOtherTools();
        rectTool.setStyle("-fx-background-image: url('/res/tools/rectWhite.png'); -fx-background-color: #0a0a0a;");
        documentList.get(currentDocumentIndex).setSelectedTool("rect");

    }

    @FXML
    private void selectOvalTool() {
        deselectOtherTools();
        ovalTool.setStyle("-fx-background-image: url('/res/tools/ovalWhite.png'); -fx-background-color: #0a0a0a;");
        documentList.get(currentDocumentIndex).setSelectedTool("oval");

    }

    private void deselectOtherTools() {

        //Clear the tool canvas.
        documentList.get(currentDocumentIndex).getToolCanvas().getGraphicsContext2D().clearRect(0, 0, documentList.get(currentDocumentIndex).getWidth(), documentList.get(currentDocumentIndex).getHeight());

        // Set the styles for all the tools to unselected.
        moveTool.setStyle("-fx-background-image: url('/res/tools/moveBlack.png'); -fx-background-color: white");
        boxSelectTool.setStyle("-fx-background-image: url('/res/tools/boxSelectBlack.png'); -fx-background-color: white;");
        pencilTool.setStyle("-fx-background-image: url('/res/tools/pencilBlack.png'); -fx-background-color: white;");
        lineTool.setStyle("-fx-background-image: url('/res/tools/lineBlack.png'); -fx-background-color: white;");
        rectTool.setStyle("-fx-background-image: url('/res/tools/rectBlack.png'); -fx-background-color: white;");
        ovalTool.setStyle("-fx-background-image: url('/res/tools/ovalBlack.png'); -fx-background-color: white;");


    }

    private void setBrushCursor() {


        Document curDoc = documentList.get(currentDocumentIndex);

        // The cursor should only change if the current tool is the pencil.
        if (curDoc.getSelectedTool().equals("pencil")) {

            //Make a circle sized to the brush.
            Circle cursorCircle = new Circle(curDoc.getBrushSize() / 2, null);
            cursorCircle.setStroke(Color.BLACK);

            //Set params for the circle snapshot.
            SnapshotParameters sp = new SnapshotParameters();
            sp.setFill(Color.TRANSPARENT);

            //Set the cursor to the image of the circle.
            Image cursorImg = cursorCircle.snapshot(sp, null);
            curDoc.getLayerList().get(curDoc.getActiveLayerIndex()).getCanvas().setCursor(new ImageCursor(cursorImg, cursorImg.getWidth() / 2, cursorImg.getHeight() / 2));

        }

    }

    // Get the value for the brush width from the slider
    @FXML
    private void updateBrushSize() {
        setCurrentDocBrushSize((int)brushSizeSlider.getValue());
        setBrushCursor();
    }

    private void setCurrentDocBrushSize(double size) {
        documentList.get(centrePane.getSelectionModel().getSelectedIndex()).setBrushSize(size);
    }

    @FXML
    private void updateColors() {

        // Set the displayed colours based on is selected.
        if (isFgColorSelected) {
            fgColorBtn.setStyle(String.format("-fx-background-color: rgb(%d,%d,%d)", (int)rSlider.getValue(), (int)gSlider.getValue(), (int)bSlider.getValue()));
        } else {
            bgColorBtn.setStyle(String.format("-fx-background-color: rgb(%d,%d,%d)", (int)rSlider.getValue(), (int)gSlider.getValue(), (int)bSlider.getValue()));
        }

        // Set the current document's foreground and background colours from the colour boxes.
        documentList.get(currentDocumentIndex).setFgColor(Color.color(Double.valueOf(getCurrentFgColour()[0]) / 255, Double.valueOf(getCurrentFgColour()[1]) / 255, Double.valueOf(getCurrentFgColour()[2]) / 255));
        documentList.get(currentDocumentIndex).setBgColor(Color.color(Double.valueOf(getCurrentBgColour()[0]) / 255, Double.valueOf(getCurrentBgColour()[1]) / 255, Double.valueOf(getCurrentBgColour()[2]) / 255));


    }

    private String[] getCurrentFgColour() {
        //Parse colours from style. *Which starts in format: -fx-background-color: rgb(0,0,0)*
        String colorStr = fgColorBtn.getStyle();

        colorStr = colorStr.substring(colorStr.indexOf("(") + 1);
        colorStr = colorStr.substring(0, colorStr.indexOf(")"));

        return colorStr.split(",");
    }

    private String[] getCurrentBgColour() {
        //Parse colours from style. *Which starts in format: -fx-background-color: rgb(0,0,0)*
        String colorStr = bgColorBtn.getStyle();

        colorStr = colorStr.substring(colorStr.indexOf("(") + 1);
        colorStr = colorStr.substring(0, colorStr.indexOf(")"));

        return colorStr.split(",");
    }

    @FXML
    private void changeSelectedColour(ActionEvent e) {

//        System.out.println("Change colour");

        if (((Button) e.getSource()).getId().equals("fg")) {
            // Style the colour buttons to indicate which is selected
            fgColorBtn.getStyleClass().setAll("colourBoxSelected");
            bgColorBtn.getStyleClass().setAll("colourBoxUnselected");
            isFgColorSelected = true;

            String[] color = getCurrentFgColour();

            //Set the spinners to the correct values.
            rSpinnerTxt.setText(color[0]);
            gSpinnerTxt.setText(color[1]);
            bSpinnerTxt.setText(color[2]);

        } else {
//            System.out.println("bg");
            fgColorBtn.getStyleClass().setAll("colourBoxUnselected");
            bgColorBtn.getStyleClass().setAll("colourBoxSelected");
            isFgColorSelected = false;

            String[] color = getCurrentBgColour();

            //Set the spinners to the correct values.
            rSpinnerTxt.setText(color[0]);
            gSpinnerTxt.setText(color[1]);
            bSpinnerTxt.setText(color[2]);

        }

    }

    //Increments and decrements for spinners.
    @FXML
    private void setSpinnerVal(ActionEvent e) {

        //Get the button that triggered the event.
        Button button = (Button) e.getSource();

        //Get the root gridpane for the spinner.
        GridPane parent = (GridPane) ((Button) e.getSource()).getParent();

        //Get the textfield for this spinner.
        TextField textField = (TextField) parent.lookup("#text");

        //Check whether to increment or decrement and do so.
        if (button.getId().equals("up")) {
            textField.setText(String.valueOf(Integer.valueOf(textField.getText()) + 1));

        } else if (button.getId().equals("down")) {
            textField.setText(String.valueOf(Integer.valueOf(textField.getText()) - 1));
        }

    }

    @FXML
    private void exportImage() {

        FileChooser chooser = new FileChooser();
        File file = chooser.showSaveDialog(stage);

        try {
            documentList.get(currentDocumentIndex).saveDocAsImg(file);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @FXML
    private void openImage() {

        FileChooser chooser = new FileChooser();
        File file = chooser.showOpenDialog(stage);

        // Create a new document from the image.
        try {
            newDocument(file);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void closeDocument(int index) {
        documentList.remove(index);
        reindexTabs();
    }

    @FXML
    private void newDocument() {

        NewDocPopup newDocWin = new NewDocPopup();

        //Make a new document using the values from a pop up.
        Document newDoc = newDocWin.show();

        if(newDoc != null) {
            documentList.add(newDoc);

            //Create a tab to hold the canvases for the document.
            Tab newTab = new Tab(newDoc.getName());

            addEventsToTab(newTab);

            //Add the tab to the tabpane.
            centrePane.getTabs().add(newTab);

            // Select the tab.
            centrePane.getSelectionModel().select(newTab);

            // Load in the canvases from the document.
            refreshCurrentTab();

        }


    }

    private void newDocument(String name, int width, int height) {

        // Create a document object and add it to the list.
        documentList.add(new Document(name, width, height));

        //Create a tab to hold the canvases for the document.
        Tab newTab = new Tab(name);

        addEventsToTab(newTab);

        //Add the tab to the tabpane.
        centrePane.getTabs().add(newTab);

        // Select the tab.
        centrePane.getSelectionModel().select(newTab);

        // Load in the canvases from the document.
        refreshCurrentTab();

    }

    private void newDocument(File file) {

        // Create a document object and add it to the list.
        try {
            documentList.add(new Document(file));
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Create a tab to hold the canvases for the document.
        Tab newTab = new Tab(file.getName());

        addEventsToTab(newTab);

        //Add the tab to the tabpane.
        centrePane.getTabs().add(newTab);

        // Select the tab.
        centrePane.getSelectionModel().select(newTab);

        // Load in the canvases from the document.
        refreshCurrentTab();

    }

    private void addEventsToTab(Tab tab) {

        // When the tab is clicked on/off refresh the current tab.
        tab.setOnSelectionChanged(event -> {
            if (centrePane.getTabs().size() != 0) {
                Platform.runLater(this::refreshCurrentTab);
            }
        });

        tab.setOnClosed(event -> {
            // If the document is the last one open, then we can't refresh the current tab.
            if (centrePane.getTabs().size() != 0) {
                closeDocument(currentDocumentIndex);
                centrePane.getSelectionModel().select(0);
                refreshCurrentTab();
            } else {
                // Ensures the document is removed from the list.
                event.consume();
                closeDocument(0);
            }
        });

    }

    @FXML
    private void newLayer() {

        // Get the current doc from the currently selected tab.
        Document currentDoc = documentList.get(centrePane.getSelectionModel().getSelectedIndex());

        currentDoc.newLayer("Layer" +  currentDoc.getLayerList().size());

        currentDoc.setActiveLayerIndex(0);

        refreshCurrentTab();

    }

    @FXML
    private void deleteLayer() {

        Document currentDoc = documentList.get(centrePane.getSelectionModel().getSelectedIndex());

        currentDoc.removeLayerByIndex(currentDoc.getActiveLayerIndex());

        refreshCurrentTab();

    }

    private void reindexTabs() {
        int index = 0;
        for(Tab tab : centrePane.getTabs()) {
            tab.setId(String.valueOf(index));
            index++;
        }

    }

    private void refreshCurrentTab() {

        currentDocumentIndex = centrePane.getSelectionModel().getSelectedIndex();

        // Get the current doc from the currently selected tab.
        Document currentDoc = documentList.get(currentDocumentIndex);

        // Make sure the doc is up to date with the UI.
        currentDoc.setShouldFillShapes(fillShapesChk.isSelected());

        //Scrollpane for canvases to be added to.
        ScrollPane canvasScrollpane = new ScrollPane();
        canvasScrollpane.setHbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        canvasScrollpane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        canvasScrollpane.setFitToWidth(true);
        canvasScrollpane.setFitToHeight(true);
        canvasScrollpane.getStyleClass().add("canvasScrollpane");


        //Gridpane for centering the canvas.
        GridPane viewportGridPane = new GridPane();
        viewportGridPane.setAlignment(Pos.CENTER);

        //GridPane that holds the canvas.
        GridPane canvasGridpane = new GridPane();
        canvasGridpane.getStyleClass().add("canvasContainer");

        //Iterate through the layers in the current document and add them to the gridpane.
        ListIterator listIterator = currentDoc.getLayerList().listIterator(currentDoc.getLayerList().size());
        ArrayList<Object> reverseLayerList = new ArrayList<>();

        while (listIterator.hasPrevious()) {
            reverseLayerList.add(listIterator.previous());
        }

        for (Object layer : reverseLayerList) {
            canvasGridpane.getChildren().add(((Layer) layer).getCanvas());
//            System.out.println(((Layer) layer).getName());
        }

        //Add toolcanvas to the current document.
        canvasGridpane.getChildren().add(currentDoc.getToolCanvas());

        viewportGridPane.getChildren().add(canvasGridpane);
        canvasScrollpane.setContent(viewportGridPane);

        centrePane.getTabs().get(centrePane.getSelectionModel().getSelectedIndex()).setContent(canvasScrollpane);

        refreshLayerList();
        refreshToolPanel();
        updateBrushSize();
        updateColors();

    }

    private void refreshLayerList() {

        // Get the current doc from the currently selected tab.
        Document currentDoc = documentList.get(centrePane.getSelectionModel().getSelectedIndex());

        // Empty the layer pane so it can be repopulated.
        layersVBox.getChildren().clear();

        //Iterate through the current document's layer list.
        for (Layer layer : currentDoc.getLayerList()) {

            //Create a hbox for the layer info.
            HBox layerHbox = new HBox();
            layerHbox.getStyleClass().add("layerHbox");
            layerHbox.setAlignment(Pos.CENTER_LEFT);

            //Button for hide/show.
            Button hideLayerBtn = new Button();
            hideLayerBtn.getStyleClass().add("hideLayerBtn");

            // Set the icon for the show/hide button.
            if (layer.isHidden()) {
                hideLayerBtn.pseudoClassStateChanged(PseudoClass.getPseudoClass("hidden"), true);
            } else {
                hideLayerBtn.pseudoClassStateChanged(PseudoClass.getPseudoClass("hidden"), false);
            }

            EventHandler<ActionEvent> showHideLayer = new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {

                    Button showHideBtn = (Button) event.getSource();

                    // Toggle the opacity of the layer.
                    Layer layer = documentList.get(currentDocumentIndex).getLayerList().get(layersVBox.getChildrenUnmodifiable().indexOf(showHideBtn.getParent()));
                    layer.setHidden(!layer.isHidden());

                    // Set the icon for the show/hide button.
                    if (layer.isHidden()) {
                        showHideBtn.pseudoClassStateChanged(PseudoClass.getPseudoClass("hidden"), true);
                    } else {
                        showHideBtn.pseudoClassStateChanged(PseudoClass.getPseudoClass("hidden"), false);
                    }

                }
            };

            hideLayerBtn.addEventHandler(ActionEvent.ACTION, showHideLayer);

            // Button that shows the layer name and also handles click events for the layer.
            Button layerLbl = new Button(layer.getName());
            HBox.setHgrow(layerLbl, Priority.ALWAYS);
            layerLbl.setMinSize(175, 1);
            layerLbl.getStyleClass().add("layerLblBtn");

            EventHandler<MouseEvent> layerClicked = new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent event) {

                    Node lblBtn = (Node) event.getSource();

                    //Check if single or double click
                    if(event.getClickCount() == 1) {

                        // Set the current document's active layer index to the id (index) of the layer that was clicked.
                        currentDoc.setActiveLayerIndex(layersVBox.getChildrenUnmodifiable().indexOf(lblBtn.getParent()));

                        //Update the visual end
                        refreshActiveLayer();

                    } else {

                        Document curDoc = documentList.get(centrePane.getSelectionModel().getSelectedIndex());

                        Layer layer = curDoc.getLayerList().get(layersVBox.getChildrenUnmodifiable().indexOf(lblBtn.getParent()));

                        // Open window and get new layer name from user.
                        layer.setName(new LayerRenamePopup().show(layer.getName()));

                        //Update visual list.
                        refreshLayerList();

                    }


                }
            };

            layerLbl.addEventHandler(MouseEvent.MOUSE_CLICKED, layerClicked);

            //Buttons for moving layer.
            Button layerUpBtn = new Button("^");
            Button layerDwnBtn = new Button("v");
            layerUpBtn.getStyleClass().add("upBtn");
            layerDwnBtn.getStyleClass().add("dwnBtn");

            //BorderPane for the layer move buttons.
            BorderPane moveBtnsPane = new BorderPane();
            moveBtnsPane.getStyleClass().add("layerMoveBtnsPane");
            moveBtnsPane.setTop(layerUpBtn);
            moveBtnsPane.setBottom(layerDwnBtn);

            //Add event listeners for the layer move buttons.
            layerUpBtn.addEventHandler(ActionEvent.ACTION, layerChangeOrderEvent);
            layerUpBtn.setId("up");
            layerDwnBtn.addEventHandler(ActionEvent.ACTION, layerChangeOrderEvent);
            layerDwnBtn.setId("down");

            //Add layer to layer vbox.
            layerHbox.getChildren().addAll(hideLayerBtn, layerLbl, moveBtnsPane);
            layersVBox.getChildren().add(layerHbox);
        }
        refreshActiveLayer();
    }

    private void refreshActiveLayer() {

        // Get the current doc from the currently selected tab.
        Document currentDoc = documentList.get(centrePane.getSelectionModel().getSelectedIndex());

        // Store the active layer index.
        int activeLayerIndex = currentDoc.getActiveLayerIndex();

        for (Node layerBox : layersVBox.getChildren()) {

            if (layersVBox.getChildren().indexOf(layerBox) == activeLayerIndex) {
                layerBox.pseudoClassStateChanged(PseudoClass.getPseudoClass("active"), true);
            } else {
                layerBox.pseudoClassStateChanged(PseudoClass.getPseudoClass("active"), false);
            }

        }

    }

    private void refreshToolPanel() {

        Document currentDoc = documentList.get(centrePane.getSelectionModel().getSelectedIndex());

        // Check the selected tool and run the relevant code.
        switch (currentDoc.getSelectedTool()) {

            case "move" : selectMoveTool(); break;
            case "boxselect" : selectBoxSelectTool(); break;
            case  "pencil" : selectPencilTool(); break;
            case  "rect" : selectRectTool(); break;
            case  "oval" : selectOvalTool(); break;

        }

    }

    //Checks if a string is an integer.
    private static boolean isInt(String str) {
        try {
            int i = Integer.parseInt(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }
}
