package Code;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;


public class PasteImgPopup {

    private String result;

    public String show() {

        try {

            FXMLLoader fxmlLoader = new FXMLLoader(this.getClass().getResource("/Layout/pasteImgWindow.fxml"));
            GridPane root = fxmlLoader.load();
            Scene scene = new Scene(root, 350, 175);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initStyle(StageStyle.UNDECORATED);
            stage.setResizable(false);
            stage.setTitle("Pasted Image Too Large");

            Label infoLbl = new Label("The image you are attempting to paste will not fit on the canvas. How would you like to proceed?");
            infoLbl.setWrapText(true);

            VBox choicesVBox = new VBox();
            choicesVBox.setSpacing(10);
            choicesVBox.setAlignment(Pos.CENTER);

            //Buttons for choices
            Button cropImgBtn = new Button("Crop Image");
            Button resizeImgBtn = new Button("Scale Image to Fit");

            // Add the buttons to the Hbox
            choicesVBox.getChildren().addAll(cropImgBtn, resizeImgBtn);

            EventHandler<ActionEvent> btnClicked = new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {

                    if (event.getSource().equals(cropImgBtn)) {
                        result = "crop";
                    } else if (event.getSource().equals(resizeImgBtn)) {
                        result = "resizeImg";
                    }

                    stage.close();

                }
            };

            cropImgBtn.addEventHandler(ActionEvent.ACTION, btnClicked);
            resizeImgBtn.addEventHandler(ActionEvent.ACTION, btnClicked);

            root.add(infoLbl, 0, 1);

            root.add(choicesVBox, 0, 2);

            stage.showAndWait();


        } catch (IOException e) {
            e.printStackTrace();
        }


        return result;
    }

}
