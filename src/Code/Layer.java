package Code;

import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.StrokeLineCap;

class Layer {

    private Canvas canvas, toolCanvas;
    private String name;
    private Document parentDoc;
    private Image snapshot;

    private double startX = 0, startY = 0, rectW, rectH, lineX, lineY, clickX = 0, clickY = 0;

    private boolean hasDragged = false;
    private boolean lineStarted = false;

    private EventHandler<MouseEvent> mouseEventHandler;

    private boolean isHidden = false;

    Layer(Document document, String name, int width, int height) {
        this.name = name;
        this.parentDoc = document;
        canvas = new Canvas(width, height);
        canvas.setFocusTraversable(true);
        GraphicsContext gc = canvas.getGraphicsContext2D();

        toolCanvas = parentDoc.getToolCanvas();
        GraphicsContext toolGC = toolCanvas.getGraphicsContext2D();

        mouseEventHandler = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {

                parentDoc.getToolCanvas().toFront();

                //Prevent from old line appearing when tool is changed.
                if (!parentDoc.getSelectedTool().equals("line")) {
                    lineStarted = false;
                }

                if (e.getEventType() == MouseEvent.MOUSE_PRESSED) {
                    parentDoc.setLocalMouseX(e.getX());
                    parentDoc.setLocalMouseY(e.getY());

                }

                //Only allow events if the layer is shown.
                if (!isHidden) {
                    switch (parentDoc.getSelectedTool()) {

                        case "move":

                            if (e.getEventType() == MouseEvent.MOUSE_PRESSED) {
                                //Set the mode of the snapshot so that it preserves transparency.
                                SnapshotParameters params = new SnapshotParameters();
                                params.setFill(Color.TRANSPARENT);

                                snapshot = canvas.snapshot(params, null);

                                canvas.setCursor(Cursor.CLOSED_HAND);

                            } else if (e.getEventType() == MouseEvent.MOUSE_DRAGGED) {

                                //Clear the canvas.
                                gc.clearRect(0, 0, gc.getCanvas().getWidth(), gc.getCanvas().getHeight());

                                //Redraw the image with the offset.
                                gc.drawImage(snapshot, e.getX() - parentDoc.getLocalMouseX(), e.getY() - parentDoc.getLocalMouseY());


                                canvas.setCursor(Cursor.CLOSED_HAND);

                            } else canvas.setCursor(Cursor.OPEN_HAND);
                            break;

                        case "boxSelect":

                            canvas.setCursor(Cursor.CROSSHAIR);

                            //Set the style for the box select line.
                            toolGC.setStroke(Color.RED);
                            toolGC.setLineDashes(10);
                            toolGC.setLineWidth(2);
                            toolGC.setLineCap(StrokeLineCap.BUTT);

                            if (e.getEventType() == MouseEvent.MOUSE_PRESSED) {

                                // Set the static point for the selection.
                                clickX = e.getX();
                                clickY = e.getY();

                                // Set the start values for the selection
                                startX = clickX;
                                startY = clickY;

                                // Clear the tool canvas
                                toolGC.clearRect(0, 0, parentDoc.getWidth(), parentDoc.getHeight());

                            } else if (e.getEventType() == MouseEvent.MOUSE_DRAGGED) {

                                // Clear the tool canvas
                                toolGC.clearRect(0, 0, parentDoc.getWidth(), parentDoc.getHeight());

                                // Set the selection dimensions (at this stage they can be negative)
                                rectW = e.getX() - clickX;
                                rectH = e.getY() - clickY;

                                if (rectW < 0) {
                                    // Set startX to the original point minus the width of the selection.
                                    startX = clickX + rectW;
                                    // Make the width +ve
                                    rectW =  -rectW;
                                }

                                if (rectH < 0) {
                                    // Set startY to the original point minus the height of the selection.
                                    startY = clickY + rectH;
                                    // Make the height +ve
                                    rectH =  -rectH;
                                }

                                toolGC.strokeRect(startX, startY, rectW, rectH);

                            }

                            parentDoc.setSelection(new Selection(startX, startY, rectW, rectH));

                            break;

                        case "pencil":

                            if (e.getEventType() == MouseEvent.MOUSE_PRESSED) {

                                gc.setStroke(parentDoc.getFgColor());
                                gc.setLineWidth(parentDoc.getBrushSize());
                                // Try to prevent jagged lines
                                gc.setMiterLimit(1);
                                gc.setLineCap(StrokeLineCap.ROUND);
                                gc.beginPath();
                                // Offset the path to try and prevent anti-aliasing
                                gc.moveTo(e.getX() + 0.5, e.getY() + 0.5);
                            } else if (e.getEventType() == MouseEvent.MOUSE_DRAGGED) {
                                gc.lineTo( e.getX() + 0.5, e.getY() - 0.5);
                                gc.stroke();
                            }
                            break;

                        case "line":

                            canvas.setCursor(Cursor.CROSSHAIR);

                            //Setup the canvases to draw a line.
                            toolGC.setStroke(parentDoc.getFgColor());
                            toolGC.setLineWidth(parentDoc.getBrushSize());
                            toolGC.setLineCap(StrokeLineCap.ROUND);
                            toolGC.setLineDashes(0);

                            gc.setStroke(parentDoc.getFgColor());
                            gc.setLineWidth(parentDoc.getBrushSize());
                            gc.setLineCap(StrokeLineCap.ROUND);

                            if (e.getEventType().equals(MouseEvent.MOUSE_CLICKED)) {

                                // Clear tool canvas.
                                toolGC.clearRect(0, 0, width, height);

                                if (e.getClickCount() == 1) {

                                    if (lineStarted) {

                                        //Draw current line to canvas.
                                        gc.strokeLine(startX, startY, lineX, lineY);

                                        //Set the start pos for new line to mouse pos.
                                        startX = e.getX();
                                        startY = e.getY();

                                    } else {

                                        //Set the start pos for new line to mouse pos.
                                        startX = e.getX();
                                        startY = e.getY();

                                        lineStarted = true;

                                    }

                                } else if (e.getClickCount() == 2) {

                                    if (lineStarted) {

                                        //Draw current line to canvas.
                                        gc.strokeLine(startX, startY, lineX, lineY);

                                        lineStarted = false;

                                    }

                                }

                            } else if (e.getEventType().equals(MouseEvent.MOUSE_MOVED)) {

                                if (lineStarted) {

                                    // Set end point for current line.
                                    lineX = e.getX();
                                    lineY = e.getY();

                                    // Clear tool canvas.
                                    toolGC.clearRect(0, 0, width, height);

                                    //Draw preview of line to tool canvas
                                    toolGC.strokeLine(startX, startY, lineX, lineY);

                                }

                            }
                            break;

                        case "rect":

                            canvas.setCursor(Cursor.CROSSHAIR);

                            //Set the style for the box select line.
                            toolGC.setStroke(parentDoc.getFgColor());
                            toolGC.setFill(parentDoc.getBgColor());
                            toolGC.setLineWidth(parentDoc.getBrushSize());
                            toolGC.setLineCap(StrokeLineCap.BUTT);
                            toolGC.setLineDashes(0);

                            gc.setMiterLimit(1000);
                            gc.setStroke(parentDoc.getFgColor());
                            gc.setFill(parentDoc.getBgColor());
                            gc.setLineWidth(parentDoc.getBrushSize());
                            gc.setLineCap(StrokeLineCap.BUTT);

                            if (e.getEventType() == MouseEvent.MOUSE_PRESSED) {

                                hasDragged = false;

                                // Set the static point for the selection.
                                clickX = e.getX();
                                clickY = e.getY();

                                // Set the start values for the selection
                                startX = clickX;
                                startY = clickY;

                                // Clear the tool canvas
                                toolGC.clearRect(0, 0, parentDoc.getWidth(), parentDoc.getHeight());

                            } else if (e.getEventType() == MouseEvent.MOUSE_DRAGGED) {

                                hasDragged = true;

                                // Clear the tool canvas
                                toolGC.clearRect(0, 0, parentDoc.getWidth(), parentDoc.getHeight());

                                // Set the selection dimensions (at this stage they can be negative)
                                rectW = e.getX() - clickX;
                                rectH = e.getY() - clickY;

                                if (rectW < 0) {
                                    // Set startX to the original point minus the width of the selection.
                                    startX = clickX + rectW;
                                    // Make the width +ve
                                    rectW =  -rectW;
                                }

                                if (rectH < 0) {
                                    // Set startY to the original point minus the height of the selection.
                                    startY = clickY + rectH;
                                    // Make the height +ve
                                    rectH =  -rectH;
                                }

//                                if (isShiftDown) rectH = rectW;

                                // Draw a preview of the rect to the tool canvas.
                                if (parentDoc.shouldFillShapes()) toolGC.fillRect(startX, startY, rectW, rectH);
                                toolGC.strokeRect(startX, startY, rectW, rectH);

                            } if (e.getEventType() == MouseEvent.MOUSE_RELEASED) {

                                if (hasDragged) {
                                    //Draw the rect on the layer.
                                    if (parentDoc.shouldFillShapes()) gc.fillRect(startX, startY, rectW, rectH);
                                    gc.strokeRect(startX, startY, rectW, rectH);
                                }

                                // Clear the tool canvas.
                                toolGC.clearRect(0, 0, width, height);

                            }
                            break;

                        case "oval":

                            canvas.setCursor(Cursor.CROSSHAIR);

                            //Set the style for the box select line.
                            toolGC.setStroke(parentDoc.getFgColor());
                            toolGC.setFill(parentDoc.getBgColor());
                            toolGC.setLineWidth(parentDoc.getBrushSize());
                            toolGC.setLineCap(StrokeLineCap.BUTT);
                            toolGC.setLineDashes(0);

                            gc.setStroke(parentDoc.getFgColor());
                            gc.setFill(parentDoc.getBgColor());
                            gc.setLineWidth(parentDoc.getBrushSize());
                            gc.setLineCap(StrokeLineCap.BUTT);

                            if (e.getEventType() == MouseEvent.MOUSE_PRESSED) {

                                hasDragged = false;

                                // Set the static point for the selection.
                                clickX = e.getX();
                                clickY = e.getY();

                                // Set the start values for the selection
                                startX = clickX;
                                startY = clickY;

                                // Clear the tool canvas
                                toolGC.clearRect(0, 0, parentDoc.getWidth(), parentDoc.getHeight());

                            } else if (e.getEventType() == MouseEvent.MOUSE_DRAGGED) {

                                hasDragged = true;

                                // Clear the tool canvas
                                toolGC.clearRect(0, 0, parentDoc.getWidth(), parentDoc.getHeight());

                                // Set the selection dimensions (at this stage they can be negative)
                                rectW = e.getX() - clickX;
                                rectH = e.getY() - clickY;

                                if (rectW < 0) {
                                    // Set startX to the original point minus the width of the selection.
                                    startX = clickX + rectW;
                                    // Make the width +ve
                                    rectW =  -rectW;
                                }

                                if (rectH < 0) {
                                    // Set startY to the original point minus the height of the selection.
                                    startY = clickY + rectH;
                                    // Make the height +ve
                                    rectH =  -rectH;
                                }

                                // Draw a preview of the rect to the tool canvas.
                                if (parentDoc.shouldFillShapes()) toolGC.fillOval(startX, startY, rectW, rectH);
                                toolGC.strokeOval(startX, startY, rectW, rectH);

                            } if (e.getEventType() == MouseEvent.MOUSE_RELEASED) {

                            if (hasDragged) {
                                //Draw the rect on the layer.
                                if (parentDoc.shouldFillShapes()) gc.fillOval(startX, startY, rectW, rectH);
                                gc.strokeOval(startX, startY, rectW, rectH);
                            }

                            // Clear the tool canvas.
                            toolGC.clearRect(0, 0, width, height);

                            }
                        break;

                    }
                }

            }
        };

        canvas.addEventHandler(MouseEvent.ANY, mouseEventHandler);
    }

    public Canvas getCanvas() {
        return canvas;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMouseTransparent(Boolean mouseTransparent) {
        canvas.setMouseTransparent(mouseTransparent);
    }

    public boolean isHidden() {
        return isHidden;
    }

    public void setHidden(boolean hidden) {

        if (hidden) {
            canvas.setOpacity(0);
        } else {
            canvas.setOpacity(1);
        }

        isHidden = hidden;
    }

}
