package Code;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.geometry.Rectangle2D;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class WindowBase {

    Stage stage;
    @FXML
    GridPane root;

    //Offsets for dragging the window
    private double stageXOffset, stageYOffset;

    private int defaultWidth, defaultHeight;

    //Set the stage variable when the mouse is moved, so it can be used later
    @FXML
    private void setupWindow() {
        stage = (Stage) root.getScene().getWindow();
    }

    @FXML
    private void closeWindow() {
        Platform.exit();
    }

    @FXML
    private void minWindow() {
        stage.setIconified(true);
    }

    @FXML
    private void maxWindow() {
//        stage.setFullScreen(!stage.isFullScreen());

        Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();

        if (stage.getWidth() < primaryScreenBounds.getWidth() || stage.getHeight() < primaryScreenBounds.getHeight()) {

            // Maximise the window
            stage.setX(primaryScreenBounds.getMinX());
            stage.setY(primaryScreenBounds.getMinY());
            stage.setWidth(primaryScreenBounds.getWidth());
            stage.setHeight(primaryScreenBounds.getHeight());
        } else {

            // Reset the window
            stage.setX(primaryScreenBounds.getWidth()/2 - defaultWidth/2);
            stage.setY(primaryScreenBounds.getHeight()/2 - defaultHeight/2);
            stage.setWidth(defaultWidth);
            stage.setHeight(defaultHeight);

        }
    }

    @FXML
    private void dragSetup(MouseEvent event) {
        stageXOffset = stage.getX() - event.getScreenX();
        stageYOffset = stage.getY() - event.getScreenY();
    }

    @FXML
    private void dragWindow(MouseEvent event) {
        //Disallow dragging of window if it's fullscreen
        if (!stage.isFullScreen()) {
            stage.setX(event.getScreenX() + stageXOffset);
            stage.setY(event.getScreenY() + stageYOffset);
        }
    }

    public void setDefaultWidth(int defaultWidth) {
        this.defaultWidth = defaultWidth;
    }

    public void setDefaultHeight(int defaultHeight) {
        this.defaultHeight = defaultHeight;
    }

    public Stage getStage() {
        return stage;
    }
}
